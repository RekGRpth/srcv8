FROM debian:stable-slim
RUN exec 2>&1 \
    && set -ex \
    && apt-get update \
    && apt-get install -y \
        git \
        curl \
        python \
    && SRC=/usr/src \
    && mkdir -p "$SRC" \
    && git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git "$SRC/depot_tools" \
    && PATH="$PATH:$SRC/depot_tools" \
    && cd "$SRC" \
    && fetch v8 \
    && cd "$SRC/v8" \
    && gclient sync \
    && apt-get remove --purge -y \
        git \
        curl \
        python \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && echo done
